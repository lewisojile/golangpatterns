package main

import (
	"fmt"

	"./adapter"
	"./models"
	_ "github.com/jinzhu/gorm/dialects/mssql"
)

//Adjust to your database usecase
var dbconf = func() (string, string) {
	//Connstr is the connection string
	connstr := "sqlserver://sa:{ replace with your sa password }@localhost:1433?database=GoDb"
	//ConnDbms is the database type
	conndbms := "mssql"
	return conndbms, connstr
}

//main entry point
func main() {

	//Start up repo
	//pull out database operations
	//Intentionally left update blank
	_, save, getall := adapter.InitRepository(dbconf)

	//create a product
	product := models.Product{
		Code:  "Mango Juice",
		Price: 5824,
	}

	//save the product
	fmt.Println(save(&product))

	//fetch the product
	fmt.Println(getall(&product))

}
