package models

import (
	"github.com/jinzhu/gorm"
)

//Product Model (adjust to usecase)
type Product struct {
	gorm.Model
	Code  string
	Price uint
}

//Order Model (adjust to usecase)
type Order struct {
	gorm.Model
	Code  string
	Price uint
}

//Imodel for model operations
type Imodel interface {
	Update(model Imodel, dbconf func() (string, string)) Imodel
	Save(model Imodel, dbconf func() (string, string)) Imodel
	GetAll(dbconf func() (string, string)) interface{}
}

//Save order
func (order Order) Save(model Imodel, dbconf func() (string, string)) Imodel {
	return order
}

//Update order
func (order Order) Update(model Imodel, dbconf func() (string, string)) Imodel {
	return order
}

//GetAll returns all orders
func (order Order) GetAll(dbconf func() (string, string)) interface{} {
	orders := []Imodel{}
	return orders
}

//Save product
func (product Product) Save(model Imodel, dbconf func() (string, string)) Imodel {

	database, connstr := dbconf()

	db, err := gorm.Open(database, connstr)
	if db == nil {
		if err != nil {
			panic(err)
		}
	}
	db.Create(model)
	defer db.Close()
	return model
}

//Update product
func (product Product) Update(model Imodel, dbconf func() (string, string)) Imodel {
	//Implement update logic here
	return model
}

//GetAll returns all products
func (product Product) GetAll(dbconf func() (string, string)) interface{} {
	products := []Product{}
	var Iproducts interface{}
	database, connstr := dbconf()
	db, err := gorm.Open(database, connstr)
	if db == nil {
		if err != nil {
			panic(err)
		}
	}

	db = db.Find(&products)
	Iproducts = make([]Product, len(products))
	Iproducts = append(Iproducts.([]Product), products...)

	defer db.Close()
	return Iproducts
}
