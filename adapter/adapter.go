package adapter

import (
	"../models"
)

//InitRepository initializes the  repository
func InitRepository(dbconf func() (string, string)) (func(model models.Imodel) models.Imodel,
	func(model models.Imodel) models.Imodel,
	func(model models.Imodel) interface{}) {

	Update := func(model models.Imodel) models.Imodel {
		return model.Update(model, dbconf)
	}
	Save := func(model models.Imodel) models.Imodel {
		return model.Save(model, dbconf)
	}
	GetAll := func(model models.Imodel) interface{} {
		return model.GetAll(dbconf)
	}

	return Update, Save, GetAll

}
